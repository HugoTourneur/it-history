-----------------------------------------------------------------------------------------------------------------------------------------------------

Début du sprint : 27 septembre 2022

But à atteindre : chaque équipe doit écrire dans un fichier texte l'histoire de l'informatique avec la date du groupe 

Marche à suivre : 

Forker https://gitlab.com/Evbrs/it-history/-/tree/main/ et rester sur la branche principale
Crée un ou plusieurs fichier(s) texte dans le dossier de votre groupe et remplissez les d'infos
Commit + Push
Merge Request
